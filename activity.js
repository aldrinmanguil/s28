db.single.insertOne({
	"name": "single",
	"accomodates": 2,
	"price": 1000,
	"description": "A simple room with all the basic necessities",
	"room_available": 10,
	"isAvailable": false
});

db.single.insertMany([
{
"name": "double",
	"accomodates": 3,
	"price": 2000,
	"description": "A room fit for samll family going on a vacation",
	"room_available": 5,
	"isAvailable": false
},
{
"name": "queen",
	"accomodates": 4,
	"price": 4000,
	"description": "A room with a queen sized bed perfect for a simple getaway",
	"room_available": 15,
	"isAvailable": false
}
	]);

db.single.find({
	"name": "double"
});

db.single.updateOne(
{
	"name": "queen"
},
{
	$set: {
		"accomodations": 0
	}
}
	);

db.single.deleteMany({
	"room_available": 0
});

